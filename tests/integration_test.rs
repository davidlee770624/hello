mod common;
// 測試 3A操作
// 1. 準備數據/狀態
// 2. cargo test 運行代碼
// 2-1. 默認並行測試
// 3. Assert 斷言結束

// /tests目錄下不需要寫#[cfg(test)], rust會識別
// /tests目錄下每一個文件都是單獨的crate, 目錄除外
// #[should_panic] 測試符合預期發生panic, 測試才算通過
// 'cargo test 函數名'
// 'cargo test --test 文件名'

// lib crate項目
// 擁有lib.rs , lib crate能爆露給/tests下的crate

// binary crate項目
// 測試放在lib.rs內
#[test]
fn test1() {
    println!("practice_15 test1");
    assert_eq!(2 + 2, 4);
}

#[test]
#[should_panic]
fn test2() {
    println!("practice_15 test2");
    assert_eq!(2 + 3, 4);
}

#[test]
fn test3() -> Result<(), String> {
    println!("practice_15 test3");
    if 2 + 2 == 4 {
        Ok(())
    } else {
        Err(String::from("測試失敗"))
    }
}

// 想要運行ignore: cargo test -- --ignored
#[test]
#[ignore]
fn test4() -> Result<(), String> {
    println!("practice_15 test4");
    assert_eq!(2 + 3, 4 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1);
}

#[test]
fn test5() {
    // 使用共用模塊代碼 做測試
    common::tool();
}