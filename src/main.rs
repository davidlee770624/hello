use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::{env, fmt, io, process, thread};
use std::io::{ErrorKind, Read};
use std::ops::Add;
use std::sync::{Arc, mpsc, Mutex};
use std::sync::atomic::AtomicBool;
use std::sync::mpsc::{Receiver, Sender};
use std::time::Duration;
use ndarray::Array2;
use rand::prelude::SliceRandom;
use rand::Rng;
use hello::redis::redis_util;

fn main() {
    // 1. 猜數字遊戲
    practice_1();

    // 2. struct練習
    practice_2();

    // 3. 計算長方體面積
    practice_3();

    // 4. enum練習
    practice_4();

    // 5. match練習
    practice_5();

    // 6. option練習
    practice_6();

    // 7. lib.rs練習
    practice_7();

    // 8. vector練習
    practice_8();

    // 9. String練習
    practice_9();

    // 10. hashmap練習
    practice_10();

    // 11. result練習
    practice_11();

    // 12. 泛型練習
    practice_12();

    // 13. Trait練習
    practice_13();

    // 14. 生命週期
    practice_14();

    // 15. 測試目錄
    // 見 /tests

    // 16. 二進制程序
    // practice_16();

    // 17. TDD練習
    // 見src/lib.rs

    // 18. 閉包
    practice_18();

    // 19. 跌代器iter
    practice_19();

    // 20. 線呈練習
    // practice_20();

    // 21. 面相對象設計
    practice_21();

    // 22. match練習
    practice_22();

    // 23. unsafe練習
    // unsafe { practice_23(); }

    // 24. trait type關聯類型練習
    practice_24();

    // 25. type練習
    practice_25();

    // 26. fn函數指針練習
    practice_26();

    // 27. 指針練習
    practice_27();

    // 28. 3X3圈圈叉叉遊戲練習
    practice_28();
}

// 猜數字遊戲
fn practice_1() {
    println!("practice_1 猜數字遊戲開始!");
    // ..= 閉區間
    let number = rand::thread_rng().gen_range(1..=100);
    println!("practice_1 神秘數字是: {}", number);

    loop {
        // let 默認immutable
        // mul 可修改的變量
        // String:: 相當於呼叫靜態方法
        let mut guess = String::new();

        // 讀取輸入
        println!("practice_1 猜一個神秘數字:");
        io::stdin().read_line(&mut guess).expect("practice_1 無法讀取");
        println!("practice_1 你猜的數字是: {}", guess);
        // guess:u32 > shadow 允許使用同變量做資料類型轉換
        // match > 常用的錯誤處裡方法
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        // 比較數值
        match guess.cmp(&number) {
            Ordering::Less => println!("practice_1 猜太小!"),
            Ordering::Greater => println!("practice_1 猜太大!"),
            Ordering::Equal => {
                println!("practice_1 猜對了!");
                break;
            }
        }
    }
}


// struct
struct User {
    user_name: String,
    login_count: u64,
    active: bool,
}

// struct
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

// impl將struct和方法關聯
impl Rectangle {
    // 關聯函數, 無&self
    fn get_square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }

    // 方法
    fn get_area(&self) -> u32 {
        let area = self.width * self.height;
        println!("practice_3 長方形面積: {}", area);
        println!("practice_3 長方形: {:#?}", self);
        area
    }
}

fn practice_3() {
    let rectangle = Rectangle {
        width: 30,
        height: 50,
    };
    rectangle.get_area();
}


// Tuple Struct
struct RGB(u64, u64, u64);

// Unit-Like Struct
// 實現trait(介面)用
struct UnitLike();

fn practice_2() {
    // 一旦賦予mut, 所有字段都是可變的
    let mut user1 = User {
        user_name: String::from("david"),
        login_count: 1,
        active: true,
    };
    println!("practice_2 user1 user_name: {}", user1.user_name);
    println!("practice_2 user1 login_count: {}", user1.login_count);
    println!("practice_2 user1 active: {}", user1.active);

    // struct更新語法
    let user2 = User {
        user_name: String::from("david2"),
        ..user1
    };
    println!("practice_2 user2 user_name: {}", user2.user_name);
}

#[derive(Debug)]
enum Ip {
    V4(IpV4),
    V6(IpV6),
}

#[derive(Debug)]
struct IpV4 {
    one: u8,
    two: u8,
    three: u8,
    four: u8,
}

#[derive(Debug)]
struct IpV6 {
    address: String,
}

impl Ip {
    // 方法
    fn print(&self) {
        let ip = self.to_owned();
        println!("practice_4 print {:#?}", ip);
    }
}

fn practice_4() {
    let ip_v4 = Ip::V4(IpV4 { one: 192, two: 168, three: 1, four: 1 });
    let ip_v6 = Ip::V6(IpV6 { address: String::from("1:2:3:4:5") });
    ip_v4.print();
    ip_v6.print();
}

fn practice_5() {
    let v = value_in_cent(Coin::USD);
    println!("practice_5 value_in_cent {}", v);
}

enum Coin {
    USD,
    TWD,
    CNY,
}

fn value_in_cent(coin: Coin) -> u8 {
    match coin {
        Coin::USD => 2,
        Coin::CNY => 2,
        _ => 0,
    }
}

fn practice_6() {
    let option = plus_one(Some(5));
    println!("practice_6 {}", option.is_some());
}

fn plus_one(value: Option<u32>) -> Option<u32> {
    match value {
        Some(v) => Some(v + 1),
        None => None,
    }
}

fn practice_7() {
    // 見 src/lib.rs
    redis_util::connection();
}

enum ExcelCell {
    Int(i32),
    Float(f64),
    Text(String),
}

fn practice_8() {
    // vector
    let v1: Vec<i32> = Vec::new();
    let mut v2 = Vec::new();
    v2.push(1);
    let v3 = vec![1, 2, 3, 4, 5];

    // panic
    let third: &i32 = &v3[2];
    println!("practice_8 vector 3 is {}", third);

    // non panic
    match v3.get(2) {
        Some(third) => println!("practice_8 vector 3 is {}", third),
        None => println!("practice_8 vector 3 no element"),
    }

    for element in &v3 {
        println!("practice_8 vector 3 element {}", element);
    }

    // 使用enum為vector增添不同變體數據
    let v4 =
        vec![ExcelCell::Int(3), ExcelCell::Text(String::from("123")), ExcelCell::Float(10.1)];
}

fn practice_9() {
    // string
    let mut s1 = String::from("123"); // 字串字面值
    let s2 = "12345";  // 字串切片
    let s3 = s2.to_string(); // 字串字面值

    let s = "1";
    let s = "2";

    let mut s4 = String::from("hello");
    s4.push_str(" world");
    println!("practice_9 String s4 {}", s4);

    let mut s5 = String::from("h");
    s5.push('i');
    println!("practice_9 String s5 {}", s5);

    let s6 = String::from("s6");
    let s7 = String::from("s7");
    let s8 = String::from("s8");
    let s9 = format!("{}-{}-{}", s6, s7, s8);
    println!("practice_9 String s9 {}", s9);

    // 此例梵語一個字母為2個字節, 切割字串必須謹慎使用, 否則panic
    let s10 = "संस्कृतम्";
    let s11 = &s10[0..3];
    println!("practice_9 {}", s11);
    // 標量質
    for s in s10.chars() {
        println!("practice_9 {}", s);
    }
}

fn practice_10() {
    // 2 vec > 1 hashmap
    let teams = vec![String::from("blue"), String::from("red")];
    let scores = vec![10, 50];
    // zip : 拉鍊
    let scores: HashMap<_, _> = teams.iter().zip(scores.iter()).collect();
    let score = scores.get(&String::from("blue"));
    match score {
        Some(s) => println!("practice_10 hashmap get {}", s),
        None => println!("practice_10 hashmap get None"),
    }
    // 遍例hashmap
    for (key, value) in scores {
        println!("practice_10 hashmap get {} {}", key, value);
    }

    // s1,s2 : string類型放入map會失效
    let s1 = String::from("blue");
    let s2 = String::from("red");
    let mut map = HashMap::new();
    map.insert(s1, s2);
    // println!("{}", s1);

    // 更新hashmap
    let mut map2 = HashMap::new();
    map2.insert(String::from("1"), 1);
    map2.entry(String::from("2")).or_insert(2);
    map2.entry(String::from("3")).or_insert(3);

    //
    let text = "hello world wonderful world";
    let mut map3 = HashMap::new();
    for word in text.split_whitespace() {
        // count 是 value的引用
        let count = map3.entry(word).or_insert(0);
        *count += 1;
    }
}

fn practice_11() {
    let file = File::open("hello.txt");
    file.unwrap_or_else(|error| match error.kind() {
        ErrorKind::NotFound => match File::create("hello.txt") {
            Ok(file) => file,
            Err(errpr) => panic!("practice_11 文件創建失敗"),
        },
        _ => panic!("practice_11 文件開啟失敗"),
    });

    let file2 = File::open("hello.txt").expect("無法打開文件");

    // 傳播錯誤
    let file = open_file();
}

fn open_file() -> Result<String, io::Error> {
    let mut file = File::open("hello.txt")?;
    // let mut f = match File::open("hello.txt") {
    //     Ok(file) => file,
    //     Err(error) => return Err(error),
    // };

    let mut str = String::new();
    file.read_to_string(&mut str)?;
    // match file.read_to_string(&mut str) {
    //     Ok(_) => Ok(str),
    //     Err(error) => Err(error),
    // }
    Ok(str)
}

fn practice_12() {
    let number_list1 = [1, 2, 3, 4, 5];
    let result1 = largest(&number_list1);
    println!("practice_12 result1 {}", result1);

    let number_list2 = ["A", "B", "C", "D", "E"];
    let result2 = largest(&number_list2);
    println!("practice_12 result2 {}", result2);

    let point = Point { x: 1, y: 2.0 };
}

// PartialOrd: 需要實現比較
fn largest<T: PartialOrd + Clone>(list: &[T]) -> &T {
    let mut largest = &list[0];
    for item in list {
        if item > largest {
            largest = item;
        }
    }
    largest
}

struct Point<T, R> {
    x: T,
    y: R,
}

impl<T, R> Point<T, R> {
    fn set(t: T, r: R) {}
}

enum MyResult<T, E> {
    Ok(T),
    Err(E),
}

fn practice_13() {
    // trait 類似介面
    let tweet = Tweet {
        author: String::from("author"),
        context: String::from("context"),
    };
    println!("practice_13 {}", tweet.summarize());

    notify1(tweet);
}

pub trait Summary {
    fn summarize(&self) -> String;

    fn summarize2(&self) -> String {
        format!("practice_13 read more {}", self.summarize())
    }
}

pub struct News {
    pub headline: String,
    pub author: String,
    pub context: String,
}

impl Summary for News {
    fn summarize(&self) -> String {
        format!("{} {} by {}", self.headline, self.context, self.author)
    }
}

pub struct Tweet {
    pub author: String,
    pub context: String,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{} by {}", self.context, self.author)
    }
}

pub fn notify1(item: impl Summary) {
    println!("practice_13 notify {}", item.summarize());
}

pub fn notify2(item: impl Summary + Display) {
    println!("practice_13 notify {}", item.summarize());
}

// trait bound
pub fn notify3<T: Summary>(item: T) {
    println!("practice_13 notify {}", item.summarize());
}

// trait bound
pub fn notify4<T: Summary>(item1: T, item2: T) {
    println!("practice_13 notify {}", item1.summarize());
}

pub fn notify5<T: Summary + Display>(item1: T, item2: T) {
    println!("practice_13 notify {}", item1.summarize());
}

pub fn notify6<T, R>(item1: T, item2: R) -> String
    where T: Summary + Display,
          R: Clone,
{
    format!("practice_13 notify {}", item1.summarize())
}

struct Pair<T> {
    x: T,
    y: T,
}

impl<T: Display + PartialOrd> Pair<T> {
    fn annother_method() {}
}

// 覆蓋實現
// 為所有實現T Trait增加 MyDisplay的方法
pub trait MyDisplay {
    fn display(&self) -> String {
        format!("practice_13 MyDisplay")
    }
}

impl<T: Display> MyDisplay for T {}

fn practice_14() {
    // 生命週期- 避免懸垂引用
    // 借用檢查器
    // let r;
    // {
    //     let x = 5;
    //     r = &x; // 失敗: x生命週期已結束
    // }
    // print!("r: {}", r);

    // 函數生命週期標註'a: 關聯生命週期,取生命週期比較短的
    // &i32 一個引用
    // &'ai32 帶有生命週期的引用
    // &'a mut i32 帶有生命週期的可變引用
    let str1 = "xxx";
    let str2 = "yyyy";
    let result = longest(str1, str2);

    // ex: 懸垂引用
    // let result = longest2(str1, str2);

    // struct生命週期標註'a
    let noval = String::from("A. B.");
    let first = noval.split('.')
        .next()
        .expect("not found");
    let i = Life {
        part: first,
    };

    // static 整個生命週期都存在
    let s: &'static str = "aaaaa";
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

// ex: 懸垂引用: 返回引用 生命週期已結束
// fn longest2<'a>(x: &'a str, y: &'a str) -> &'a str {
//     let result = String::from("abc");
//     result.as_str() // 懸垂引用
// }

// ex: 懸垂引用: 返回值 生命週期移交給函數調用者
// fn longest3<'a>(x: &'a str, y: &'a str) -> String {
//     let result = String::from("abc");
//     result
// }

struct Life<'a> {
    part: &'a str,
}

impl<'a> Life<'a> {
    fn level(&self) -> i32 {
        3
    }
}

// 需使用命令帶兩個參數 cargo run t1 t2
fn practice_16() {
    let args: Vec<String> = env::args().collect();
    let flag: Flag = crate::Flag::new(&args).unwrap_or_else(|err| {
        println!("parse flag fail {}", err);
        process::exit(1);
    });
    println!("practice_16 {:#?}", flag);
}

#[derive(Debug)]
struct Flag {
    flag1: String,
    flag2: String,
}

impl Flag {
    fn new(args: &[String]) -> Result<Flag, &str> {
        if args.len() != 3 {
            return Err("執行需2個參數");
        }
        let flag1 = args[1].clone();
        let flag2 = args[2].clone();
        let flag = Flag {
            flag1: flag1,
            flag2: flag2,
        };
        Ok(flag)
    }
}


fn practice_18() {
    // 閉包|num|
    let mut cacher = Cacher::new(|num| {
        num + 1
    });
    // cacher僅執行第一次
    let v1 = cacher.value(10);
    println!("practice_18 v1 ={}", v1);
    let v2 = cacher.value(20);
    println!("practice_18 v2 ={}", v2);


    // move
    // let x = vec![1, 2, 3];
    // let y = move |num| num == x;
    // println!("move {:?}", x); // x所有權已被移動
}

struct Cacher<T>
    where T: Fn(u32) -> u32,
{
    calculation: T,
    // 計算邏輯
    value: Option<u32>, // 計算結果
}

impl<T> Cacher<T>
    where T: Fn(u32) -> u32,
{
    fn new(calculation: T) -> Cacher<T> {
        Cacher {
            calculation: calculation,
            // 計算邏輯
            value: None,
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            Some(v) => v,
            None => {
                let v = (self.calculation)(arg);
                self.value = Some(v);
                v
            }
        }
    }
}

fn practice_19() {
    // 跌帶器是lazy的
    let v1 = vec![1, 2, 3];
    for item in v1.iter() {
        println!("practice_19 v1 item {}", item);
    }

    // next()
    let v2 = vec![1, 2, 3];
    let mut v2_iter = v2.iter();
    println!("practice_19 v2 item {:?}", v2_iter.next());
    println!("practice_19 v2 item {:?}", v2_iter.next());

    // map
    let v3 = vec![1, 2, 3];
    let v4: Vec<i32> = v3.iter()
        .filter(|item| item >= &&2)
        .map(|item| item + 1)
        .collect();
}

fn practice_20() {
    // 多線呈
    // join(): 主線呈等待子線呈結束
    let joinHandle = thread::spawn(|| {
        for i in 1..10 {
            println!("practice_20 subThread {}", i);
            thread::sleep(Duration::from_millis(1));
        }
    });
    for i in 1..3 {
        println!("practice_20 thread {}", i);
        thread::sleep(Duration::from_millis(1));
    }
    joinHandle.join().unwrap();

    // move , 主線呈可能比子現呈早先drop
    let v = vec![1, 2, 3];
    let handle = thread::spawn(move || {
        println!("practice_20 subThread {:?}", v);
    });
    handle.join().unwrap();

    // 消息傳遞
    let (sender, receiver) = mpsc::channel();
    // 多個發送者
    let sender1 = mpsc::Sender::clone(&sender);
    let sender2 = mpsc::Sender::clone(&sender);
    thread::spawn(move || {
        sender1.send(String::from("sender1: hello")).unwrap();
        sender1.send(String::from("sender1: world")).unwrap();
        sender1.send(String::from("sender1: my")).unwrap();
        sender1.send(String::from("sender1: name")).unwrap();
        sender1.send(String::from("sender1: is")).unwrap();
        sender1.send(String::from("sender1: david")).unwrap();
    });
    thread::spawn(move || {
        sender2.send(String::from("sender2: hello")).unwrap();
        sender2.send(String::from("sender2: world")).unwrap();
        sender2.send(String::from("sender2: my")).unwrap();
        sender2.send(String::from("sender2: name")).unwrap();
        sender2.send(String::from("sender2: is")).unwrap();
        sender2.send(String::from("sender2: david")).unwrap();
    });

    // let msg = receiver.recv().unwrap(); // 阻塞等待
    // receiver.try_recv() 不等待阻塞
    for msg in receiver {
        println!("practice_20 msg {:?}", msg);
    }

    // send trait 和 sync trait
    // send trait: 允許線呈間轉移所有權
    // sync trait: 允許多線呈訪問
}

fn practice_21() {
    // dyn trait
    let screens = Screen {
        components: vec![
            Box::new(Square {
                width: 10,
                height: 5,
            }),
            Box::new(Circle {
                radius: 10,
            }),
        ]
    };
    screens.run();

    // 面向對象設計
}

pub trait Draw {
    fn draw(&self);
}

pub struct Screen {
    pub components: Vec<Box<dyn Draw>>, // 實現Draw都可以放入
}

impl Screen {
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}

pub struct Square {
    width: u32,
    height: u32,
}

impl Draw for Square {
    fn draw(&self) {
        print!("practice_21 Draw Square");
    }
}

pub struct Circle {
    radius: u32,
}

impl Draw for crate::Circle {
    fn draw(&self) {
        print!("practice_21 Draw Circle");
    }
}

pub struct Post {
    state: Option<Box<dyn State>>,
    content: String,
}

trait State {
    fn review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) -> Box<dyn State>;
    fn content<'a>(&'a self, post: &'a Post) -> &str {
        &post.content
    }
}

fn practice_22() {
    // match & option

    // while let
    let mut stack = Vec::new();
    stack.push(1);
    stack.push(2);
    stack.push(3);
    while let Some(top) = stack.pop() {
        println!("{}", top);
    }

    // for
    let values = vec!['a', 'b', 'c'];
    for (index, value) in values.iter().enumerate() {
        println!("index:{} , value:{}", index, value);
    }

    // let
    let (x, y, z) = (1, 2, 3);

    // fn
    let point = (3, 5);
    print(&point);
}

fn print((x, y): &(i32, i32)) {
    println!("x:{} , y:{}", x, y);
}


static mut COUNT: u32 = 0;

unsafe fn practice_23() {
    // 封裝unsafe塊代碼
    // 1. 指針
    let mut num = 5;
    //  // '&num' 是一個對 num 的不可變引用, 使用 as *const i32 將其轉換為指向 i32 型別的不可變原始指標
    let r1 = &num as *const i32; // 創建了一個指向 num 的不可變原始指標 r1
    // '&mut num' 是一個對 num 的可變引用, 使用 as *mut i32 將其轉換為指向 i32 型別的可變原始指標
    let r2 = &mut num as *mut i32; // 創建了一個指向 num 的可變原始指標 r2
    unsafe {
        print!("practice_23 r1 {}", *r1); // 解引用原始指針 不安全
        print!("practice_23 r2 {}", *r2); // 解引用原始指針 不安全
    }

    // 指針
    let address = 0x012345usize;
    let r3 = address as *const i32;
    unsafe {
        print!("practice_23 r3 {}", *r3); // 解引用原始指針 不安全
    }

    // unsafe方法
    unsafe {
        dangerous();
    }

    // extern
    unsafe {
        println!("practice_23 abs {}", abs(-3));
    }

    // static全局變量
    add_count(1);
}

unsafe fn dangerous() {}

extern "C" {
    fn abs(input: i32) -> i32;
}

#[no_mangle]
pub extern "C" fn call_from_c() {
    println!("practice_23 call from c");
}

fn add_count(count: u32) {
    unsafe {
        COUNT += count;
    }
}

fn practice_24() {
    // type: 關聯類型只能實現一次
    let count = Counter {};

    // 重載
    let human = Human {};
    human.fly();
    Wizard::fly(&human);

    // 完全限定語法
    // <Trait>::function(method , arg...);
    <Human as Wizard>::name();

    // super trait
    // MyPrint{}
}

pub trait Iter {
    type Item; // 關聯類型

    fn next(&mut self) -> Option<Self::Item>;
}

struct Counter {}

impl Iter for Counter {
    type Item = u32; // 指名關聯類型

    fn next(&mut self) -> Option<Self::Item> {
        todo!()
    }
}

struct MapPoint {
    x: i32,
    y: i32,
}

impl Add for MapPoint {
    type Output = MapPoint; // 重載

    fn add(self, other: Self) -> Self::Output {
        MapPoint {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

struct Human {}

impl Human {
    fn fly(&self) {
        println!("practice_24 human fly");
    }
}

trait Wizard {
    fn fly(&self);

    fn name();
}

impl Wizard for Human {
    fn fly(&self) {
        println!("practice_24 Wizard fly");
    }

    fn name() {
        println!("practice_24 Wizard name");
    }
}

// 實現MyPrint者, 需實現Display
trait MyPrint: Display {
    fn print(&self) {
        let output = self.to_string();
        println!("{}", output);
    }
}

impl Display for Human {
    // 實現MyPrint者, 需實現Display
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        todo!()
    }
}

impl MyPrint for Human {}

type Kilometers = i32;
type Alias = Box<dyn Fn() + Send + 'static>;
type MyResult2<T> = Result<T, io::Error>;

fn practice_25() {
    // Kilometers
    let x: i32 = 5;
    let y: Kilometers = 10;
    println!("x+y={}", x + y);

    // Alias
    let f: Alias = Box::new(|| println!("practice_25"));

    // sized trait
    // 大小確定
}

trait Test {
    fn write() -> MyResult2<usize>;
}

// Sized: 大小確定
// ?:可能是Sized, 或不是Sized
fn dynamic_sized<T: ?Sized>() {}


fn practice_26() {
    // fn函數指針
    let result = do_twice(add_one, 1);
    println!("practice_26 result={}", result);

    // 閉包
    let numbers = vec![1, 2, 3, 4, 5];
    let numbers_strings: Vec<_> = numbers.iter()
        .map(|number| number.to_string())
        .collect();

    // 返回閉包
    let fns = getFn();
}

fn do_twice(f: fn(i32) -> i32, num: i32) -> i32 {
    f(num) + f(num)
}

fn add_one(x: i32) -> i32 {
    x + 1
}

fn getFn() -> Box<dyn Fn(i32) -> i32> {
    Box::new(|x| x + 1)
}

fn practice_27() {
    // 指針: 數值指針不相同:copy trait
    let x = 42; // 定义一个整数变量 x
    let ptr_x = &x as *const i32; // 获取变量 x 的指针，并转换为指向 i32 类型的不可变原始指针
    let y = 42; // 定义一个整数变量 x
    let ptr_y = &y as *const i32; // 获取变量 x 的指针，并转换为指向 i32 类型的不可变原始指针
    // 打印变量 x 的内存地址
    println!("practice_27 Memory address of x: {:?}", ptr_x);
    println!("practice_27 Memory address of y: {:?}", ptr_y);

    // 指針: 切面指針會相同
    let x = "hi"; // 定义一个整数变量 x
    let ptr_x = x.as_ptr(); // 获取变量 x 的指针，并转换为指向 i32 类型的不可变原始指针
    let y = "hi"; // 定义一个整数变量 x
    let ptr_y = y.as_ptr(); // 获取变量 x 的指针，并转换为指向 i32 类型的不可变原始指针
    // 打印变量 x 的内存地址
    println!("practice_27 Memory address of x: {:?}", ptr_x);
    println!("practice_27 Memory address of y: {:?}", ptr_y);
}

fn practice_28() {
    // 新棋局
    // Arc 允許多個所有者共享相同的數據
    // Mutex 多個所有者互斥鎖
    let board = Arc::new(Mutex::new(Board::new()));
    // 創建了一個新的指針，這個指針指向與原始指針相同的數據
    let board_2 = Arc::clone(&board);
    let board_3 = Arc::clone(&board);

    let (player1_sender, player1_receiver) = mpsc::channel();
    let (player2_sender, player2_receiver) = mpsc::channel();
    let player1_sender_clone = player1_sender.clone();

    println!("practice_28 開始");
    // move移動了
    let player1_thread = thread::spawn(move || {
        play_game(&player1_receiver, board, &Grid::X, &player2_sender);
        println!("player1 結束");
    });
    // move移動了

    let player2_thread = thread::spawn(move || {
        play_game(&player2_receiver, board_2, &Grid::O, &player1_sender);
        println!("player2 結束");
    });
    player1_sender_clone.send(String::from("practice_28 開始")).expect("TODO: panic message");

    player1_thread.join();
    player2_thread.join();
    println!("practice_28 結束 {:?}", &board_3);
}

fn play_game(receiver: &Receiver<String>, board: Arc<Mutex<Board>>, new_grid: &Grid, sender: &Sender<String>) {
    let mut is_game_over = false;
    while !is_game_over {
        {
            // 等待
            match receiver.recv() {
                Ok(String) => {}
                Err(err) => println!("receiver 失敗 {} {:?}", err, new_grid),
            }

            // 離開作用解鎖
            let mut board_lock = board.lock().expect("TODO: panic message");
            if board_lock.is_game_over {
                is_game_over = board_lock.is_game_over;
            } else {
                board_lock.add_grid(new_grid);
            }
            // 通知
            match sender.send(String::from("換人")) {
                Ok(String) => {}
                Err(err) => println!("sender 失敗 {} {:?}", err, new_grid),
            }
        }
    }
}

#[derive(Debug)]
struct Board {
    is_game_over: bool,
    grids: Array2<Grid>,
}

impl Board {
    fn new() -> Board {
        Board {
            is_game_over: false,
            grids: Array2::<Grid>::from_elem((3, 3), Grid::Empty),
        }
    }

    fn add_grid(&mut self, player: &Grid) {
        let mut grids = &mut self.grids;
        let empty_positions: Vec<(usize, usize)> = grids.indexed_iter()
            .filter(|(_, &grid)| grid == Grid::Empty)
            .map(|(index, _)| index)
            .collect();

        // 剩餘空格
        if empty_positions.is_empty() {
            self.is_game_over = true;
            return;
        }
        // 下子
        let len = empty_positions.len();
        let random_number = rand::thread_rng().gen_range(0..len);
        let new_position = empty_positions[random_number];
        let row = new_position.0;
        let col = new_position.1;
        grids[[row, col]] = *player;
        println!("下子 {:?} {} {}", *player, row, col);

        // 檢查垂直
        if *player == grids[[row, 0]]
            && *player == grids[[row, 1]]
            && *player == grids[[row, 2]] {
            println!("下子 結束");
            self.is_game_over = true;
            return;
        }
        // 檢查列
        if *player == grids[[0, col]]
            && *player == grids[[1, col]]
            && *player == grids[[2, col]] {
            println!("下子 結束");
            self.is_game_over = true;
            return;
        }
        // 檢查對角線
        // 略
        return;
    }
}

#[derive(Debug, Copy)]
#[derive(Clone)]
#[derive(PartialEq)]
enum Grid {
    Empty,
    X,
    O,
}