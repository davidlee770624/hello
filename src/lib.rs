pub mod redis;
pub mod rabbitmq;


// #[cfg(test)] 詮釋會告訴 Rust 當你執行 cargo test 才會編譯並執行測試程式碼。而不是當你執行 cargo build
// TDD
#[cfg(test)]
mod tests {
    use std::ops::Add;
    use super::*;

    #[test]
    fn test1() {
        let query = "david";
        let content = r#"hello world
my name is david"#;
        assert_eq!(vec!["my name is david"], search(query, content));
    }
}

pub fn search<'a>(query: &str, content: &'a str) -> Vec<&'a str> {
    let mut result = Vec::new();
    for line in content.lines() {
        if line.contains(query) {
            result.push(line);
        }
    }
    result
}